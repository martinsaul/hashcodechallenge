package io.saul.google.challenge.pizza;

import io.saul.google.challenge.pizza.io.IOHandler;
import io.saul.google.challenge.pizza.io.ReadResult;

import java.io.File;
import java.io.IOException;

public class Launcher {

    public static void main(String[] args){
        if(args.length == 0){
            System.err.println("Missing path to target file");
            System.exit(-1);
        }
        String path = String.join(" ", args);
        if(args.length > 1)
            path = "\"" + path + "\"";
        File targetFile = new File(path);

        if(targetFile.isFile()){
            processFile(targetFile);
        } else if(targetFile.isDirectory()){
            for(File file: targetFile.listFiles()){
                if(file.getName().endsWith(".in") && file.isFile())
                    processFile(file);
                else
                    System.out.println("Ignoring file: " + file.getAbsolutePath());
            }
        } else {
            System.err.println("Not a valid file or folder: " + targetFile.getAbsolutePath());
            System.exit(-1);
        }
    }

    private static void processFile(File targetFile) {
        System.out.println("Reading file: " + targetFile.getAbsolutePath());
        int maxSlice = -1;
        Integer[] typeSize = null;
        try {
            ReadResult rr = IOHandler.read(targetFile);

            maxSlice = rr.getMaxSlice();
            typeSize = rr.getTypeSize();
        } catch (IOException exp){
            System.err.println("Failed to read file " + targetFile.getAbsolutePath());
            exp.printStackTrace();
            System.exit(-1);
        }

        System.out.println("Read file successfully. Processing...");

        try {
            IOHandler.write(targetFile, PizzaBoy.process(maxSlice, typeSize));
        } catch (IOException e) {
            System.err.println("Failed to write result file for " + targetFile.getAbsolutePath());
            e.printStackTrace();
            System.exit(-1);
        }
    }

}
