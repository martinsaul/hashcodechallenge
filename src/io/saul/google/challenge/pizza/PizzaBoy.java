package io.saul.google.challenge.pizza;

import java.util.*;

public class PizzaBoy {
    public static Integer[] process(final int maxSlice, final Integer[] typeSize) {
        HashMap<Integer, Integer[]> resultMap = new HashMap<>();
        List<Integer> typeSizeAL = Arrays.asList(typeSize);

        System.out.println("Sorting input...");
        Collections.sort(typeSizeAL);

        System.out.println("Seeking valid results...");
        // This is Nˆ2. It can be optimized to n*Log(n)
        for(int pos = typeSizeAL.size(); pos >= 0; pos--)
            getResult(maxSlice, typeSizeAL.subList(0,pos), resultMap);

        final ArrayList<Integer> list = new ArrayList<>(resultMap.keySet());
        Collections.sort(list);
        final Integer highestResult = list.get(list.size() - 1);
        System.out.println("Found highest result as: " + highestResult + " out of " + maxSlice + ".");

        return resultMap.get(highestResult);
    }

    private static void getResult(int maxSlice, List<Integer> typeSizeAL, HashMap<Integer, Integer[]> resultMap) {
        ArrayList<Integer> resultList = new ArrayList<>();
        int currentCount = maxSlice;

        for(int pos = typeSizeAL.size() - 1; pos >= 0; pos--){
            int number = typeSizeAL.get(pos);
            if(number <= currentCount){
                resultList.add(0, pos);
                currentCount -= number;
            }
        }

        //noinspection ToArrayCallWithZeroLengthArrayArgument
        resultMap.put(maxSlice - currentCount, resultList.toArray(new Integer[resultList.size()]));
    }
}
