package io.saul.google.challenge.pizza.io;

import java.io.*;

public class IOHandler {
    public static ReadResult read(File targetFile) throws IOException {
        BufferedReader br = new BufferedReader(new FileReader(targetFile));

        int maxSlice = Integer.parseInt(br.readLine().split(" ")[0]);
        final String[] list = br.readLine().split(" ");
        final Integer[] result = new Integer[list.length];
        for(int index = 0; index < list.length; index++){
            result[index] = Integer.parseInt(list[index]);
        }

        return new ReadResult(maxSlice, result);
    }

    public static void write(File targetFile, Integer[] process) throws IOException {
        System.out.println("Writing file...");
        File resultFile = new File(targetFile.getParentFile(), targetFile.getName() + ".out");
        BufferedWriter writer = new BufferedWriter(new FileWriter(resultFile));
        writer.write(Integer.toString(process.length));
        writer.newLine();
        boolean writeSpace = false;
        for(Integer num: process) {
            if(writeSpace) {
                writer.write(" ");
            } else writeSpace = true;
            writer.write(Integer.toString(num));
        }

        writer.close();
        System.out.println("File write complete.");
    }
}
