package io.saul.google.challenge.pizza.io;

public final class ReadResult {
    private final int maxSlice;
    private final Integer[] typeSize;

    public ReadResult(int maxSlice, Integer[] typeSize){
        this.maxSlice = maxSlice;
        this.typeSize = typeSize;
    }

    public int getMaxSlice() {
        return maxSlice;
    }

    public Integer[] getTypeSize() {
        return typeSize;
    }
}
